const expressMailer = require('express-mailer');
const keys = require('../config/keys');

module.exports = app => {
  expressMailer.extend(app, {
    from: 'no-reply@example.com',
    host: 'smtp.gmail.com',
    secureConnection: true,
    port: 465,
    transportMethod: 'SMTP',
    auth: {
      user: keys.mailUserName,
      pass: keys.mailUserPassword
    }
  });
};