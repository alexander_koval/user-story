const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const { comparePassword } = require('../utils/hashing');

const User = mongoose.model('User');

passport.serializeUser((user, done) => {
  done(null, user.id)
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    if (err) {
      return done(err)
    }

    done(null, user)
  })
});

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  }, async (email, password, done) => {
    await User.findOne({email: email}, (err, user) => {
      if (err) {
        return done(err)
      }
      if (!user) {
        return done(null, false)
      }
      if (comparePassword(user.password, password) === false) {
        return done(null, false)
      }

      return done(null, user);
    });
  }
));
