const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
  hashPassword: password => {
    return bcrypt.hashSync(password, saltRounds)
  },
  comparePassword: (dbPassword, checkPassword) => {
    return bcrypt.compareSync(checkPassword, dbPassword);
  }
};