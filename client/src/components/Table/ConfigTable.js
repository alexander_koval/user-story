import React from "react";
import { connect } from "react-redux"
import PropTypes from "prop-types";
import { moveUpCategory, moveDownCategory } from "../../actions";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components
import tableStyle from "assets/jss/material-dashboard-react/components/tableStyle.jsx";
import ArrowUpWard from "@material-ui/icons/ArrowUpward";
import ArrowDownWard from "@material-ui/icons/ArrowDownward";
import Button from "../CustomButtons/Button";
import CustomDialog from "../dialog/CustomDialog";
import CustomModal from "../modals/CustomModal";
import Typography from "@material-ui/core/Typography/Typography";
import UpdateModal from "../modals/UpdateModal";

const ConfigTable = ({ ...props }) => {
  const { classes, tableHead, tableData, tableHeaderColor } = props;
  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
            <TableRow>
              {tableHead.map((prop, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    key={key}
                  >
                    {prop}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {tableData.map((category) => {
            return (
              <TableRow key={category._id}>
                <TableCell className={classes.tableCell} key={category._id}>
                  <UpdateModal
                    id={category._id}
                    name={category.values ? category.values.name : ""}
                  />
                </TableCell>
                <TableCell>
                  <Button
                    color="info"
                    onClick={() => props.moveUpCategory(category._id)}
                  >
                    <ArrowUpWard/>
                  </Button>
                  <Button
                    color="info"
                    onClick={() => props.moveDownCategory(category._id)}
                  >
                    <ArrowDownWard/>
                  </Button>
                  <CustomDialog
                    id={category._id}
                    name={category.values ? category.values.name : ""}
                  />
                  <CustomModal>
                    <Typography variant="subheading" id="modal-title">
                      Text in a modal
                    </Typography>
                    <Typography variant="subheading" id="simple-modal-description">
                      Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                    </Typography>
                  </CustomModal>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </div>
  );
};

ConfigTable.defaultProps = {
  tableHeaderColor: "gray"
};

ConfigTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.object)
};

export default withStyles(tableStyle)(connect(null, {moveUpCategory, moveDownCategory})(ConfigTable));
