import React from "react";

const ErrorField = ({ error }) => {
  return (
    <div style={{ color: "red", marginLeft: "2rem" }}>
      {error ? error : null}
    </div>
  );
};

export default ErrorField;
