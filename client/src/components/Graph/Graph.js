import _ from 'lodash';
import Chartist from 'chartist';
import React from 'react';
import ChartistGraph from 'react-chartist';
import GridContainer from '../Grid/GridContainer';
import GridItem from '../Grid/GridItem';
import { completedTasksChart } from '../../variables/charts.jsx';

const Graph = ({ expenses }) => {
  const labelsExpenses = _.map(expenses, 'category');
  const seriesExpenses = _.map(expenses, 'sum');
  const maxHeight = _.max(seriesExpenses) || 0;
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <div style={{ marginTop: '2rem' }}>
          <ChartistGraph
            style={{ backgroundColor: 'orangered', height: '30rem' }}
            className="ct-chart"
            data={{ labels: labelsExpenses, series: [seriesExpenses] }}
            type="Bar"
            options={{
              lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
              }),
              low: 0,
              high: maxHeight,
              chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
              }
            }}
            listener={completedTasksChart.animation}
          />
        </div>
      </GridItem>
    </GridContainer>
  );
};

export default Graph;
