import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { registerUser } from "../../actions";
import { validateSignPage } from "../../utils/validateSignPage";

import CustomInput from "../fields/CustomInput";
import Button from "../CustomButtons/Button.jsx";
import CardBody from "../Card/CardBody.jsx";
import CardFooter from "../Card/CardFooter.jsx";
import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import ErrorField from "../fields/ErrorField";

const SignForm = ({handleSubmit, registerUser, history, auth, invalid}) => {
  return (
    <form onSubmit={handleSubmit(values => registerUser(values, history))}>
      <CardBody>
        <ErrorField error={auth.error}/>
        <GridContainer>
          <GridItem xs={12} sm={12} md={8}>
            <Field
              name="email"
              labelText="Email address"
              component={CustomInput}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "email"
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={8}>
            <Field
              name="password"
              labelText="Password"
              component={CustomInput}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "password"
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={8}>
            <Field
              name="confirm_password"
              labelText="Repeat password"
              component={CustomInput}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "password"
              }}
            />
          </GridItem>
        </GridContainer>
      </CardBody>
      <CardFooter>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Button type="submit" disabled={invalid} color="primary">
              Sign up
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={12}>
            <Link to="/login">Already have account? Sign in</Link>
          </GridItem>
        </GridContainer>
      </CardFooter>
    </form>
  );
};


function mapStateToProps({auth}) {
  return {auth}
}

const SignFormWithConnect = connect(mapStateToProps, {registerUser})(withRouter(SignForm));

export default reduxForm({
  form: "sign",
  validate: validateSignPage
})(SignFormWithConnect);
