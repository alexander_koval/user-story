import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { validateLoginPage } from "../../utils/validateLoginPage";
import { loginUser } from "../../actions";

import CardBody from "../Card/CardBody";
import GridContainer from "../Grid/GridContainer";
import GridItem from "../Grid/GridItem";
import CustomInput from "../fields/CustomInput";
import CardFooter from "../Card/CardFooter";
import Button from "../CustomButtons/Button";
import ErrorField from "../fields/ErrorField";

const LoginForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit(values => props.loginUser(values, props.history))}>
      <CardBody>
        <ErrorField error={props.auth.error}/>
        <GridContainer>
          <GridItem xs={12} sm={12} md={8}>
            <Field
              name="email"
              labelText="Email Address"
              component={CustomInput}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "email"
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={8}>
            <Field
              name="password"
              labelText="Password"
              component={CustomInput}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "password"
              }}
            />
          </GridItem>
        </GridContainer>
      </CardBody>
      <CardFooter>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Button type="submit" color="primary" disabled={props.invalid}>
              Sign in
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={12}>
            <Link to="/sign">First time user? Sign up</Link>
          </GridItem>
        </GridContainer>
      </CardFooter>
    </form>
  );
};

function mapStateToProps({auth}) {
  return {auth}
}

export default reduxForm({
  form: "login",
  validate: validateLoginPage
})(connect(mapStateToProps, {loginUser})(withRouter(LoginForm)));
