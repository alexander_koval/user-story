import React from "react";
import { connect } from "react-redux";
import { updateCategory } from "../../actions";
import { Field, reduxForm } from "redux-form";

import CustomInput from "../fields/CustomInput";
import GridItem from "../Grid/GridItem";
import GridContainer from "../Grid/GridContainer";
import Button from "../CustomButtons/Button";

const UpdateNameForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit(values => {props.updateCategory(values, props.id); props.handleClose()})}>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Field
            name="name"
            labelText="Enter category name"
            component={CustomInput}
            formControlProps={{
              fullWidth: true
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Button type="submit" color="primary">
            Add category name
          </Button>
        </GridItem>
      </GridContainer>
    </form>
  );
};

export default reduxForm({
  form: "uname"
})(connect(null, {updateCategory})(UpdateNameForm));
