import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import queryString from "query-string";
import { verifyUser } from "../../actions";

import GridItem from "../Grid/GridItem.jsx";
import GridContainer from "../Grid/GridContainer.jsx";
import CardBody from "../Card/CardBody.jsx";
import CardFooter from "../Card/CardFooter.jsx";
import CustomInput from "../fields/CustomInput";
import Button from "../CustomButtons/Button";
import ErrorField from "../fields/ErrorField";

const VerifyForm = props => {
  const parsed = queryString.parseUrl(props.location.search);
  const { query } = parsed;

  return (
    <form
      onSubmit={props.handleSubmit(() =>
        props.verifyUser(query, props.history)
      )}
    >
      <CardBody>
        <ErrorField error={props.auth.error} />
        <GridContainer>
          <GridItem xs={12} sm={12} md={8}>
            <Field
              name="email"
              labelText={query.email || ""}
              component={CustomInput}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "email",
                disabled: true
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={8}>
            <Field
              name="checkcode"
              labelText={query.check || ""}
              component={CustomInput}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "password",
                disabled: true
              }}
            />
          </GridItem>
        </GridContainer>
      </CardBody>
      <CardFooter>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Button type="submit" disabled={props.invalid} color="primary">
              Verify Email
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={12}>
            <Link to="/login">Already have account? Sign in</Link>
          </GridItem>
        </GridContainer>
      </CardFooter>
    </form>
  );
};

function mapStateToProps({ auth }) {
  return { auth };
}

export default reduxForm({
  form: "verify"
})(
  connect(
    mapStateToProps,
    { verifyUser }
  )(withRouter(VerifyForm))
);
