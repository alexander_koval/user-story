import _ from "lodash";
import React, { Component } from "react";
import { Field, reduxForm, reset } from "redux-form";
import withStyles from "@material-ui/core/styles/withStyles";
import { connect } from "react-redux";
import { addExpense, fetchExpenses } from "../../actions";

import GridItem from "../Grid/GridItem";
import CustomInput from "../fields/CustomInput";
import CustomSelect from "../fields/CustomSelect"
import Autocomplete from "../Autocomplete/Autocomplete";
import Button from "../CustomButtons/Button";
import GridContainer from "../Grid/GridContainer";

const styles = {
  formControl: {
    paddingBottom: "10px",
    margin: "27px 0 0 0",
    position: "relative"
  }
};

class ExpenseForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submit: false
    }
  }

  setSubmitting = () => {
    this.setState({submit: true});
    setTimeout(() => {this.setState({submit: false})}, 1000);
  };

  setDescription = (desc) => {
    this.props.change("description", desc)
  };

  renderOption() {
    if(this.props.categories.length === 0) {
      return null;
    } else {
      return this.props.categories.map(category => {
        return (
          <option
            key={category._id}
            value={category.values ? category.values.name : ""}
          >
            {category.values ? category.values.name : ""}
          </option>
        );
      });
    }
  }

  getSuggestions() {
    return _.chain(this.props.expenses)
      .map("description")
      .compact()
      .uniq()
      .map(item => ({"description": item}))
      .value();

  }

  render() {
    return (
      <form
        autoComplete="off"
        onSubmit={
          this.props.handleSubmit(value => {
            this.props.addExpense(value);
            this.props.fetchExpenses();
            this.setSubmitting();
          })}
      >
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <Field
              initialValue={"45"}
              name="category"
              component={CustomSelect}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: this.state.onchangeValue,
                native: true
            }}
            >
              {this.renderOption()}
            </Field>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <Field
              name="description"
              labelText="Description"
              component={Autocomplete}
              suggestions={this.getSuggestions()}
              setDescription={this.setDescription}
              submit={this.state.submit}
              formControlProps={{
                fullWidth: true
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={2}>
            <Field
              name="sum"
              labelText="Value UAH"
              component={CustomInput}
              formControlProps={{
                fullWidth: true
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={2}>
            <Button
              className={this.props.classes.formControl}
              type="submit"
              color="primary"
            >
              Add expenses
            </Button>
          </GridItem>
        </GridContainer>
      </form>
    );
  }
}

const validate = values => {
  const errors = {};

  if(!values.sum) {
    errors.sum = "This field is required!"
  }

  let theSum = parseFloat(values.sum);

  if(isNaN(theSum)) {
    errors.sum = "Value must be a number!"
  }

  return errors
};

const afterSubmit = (result, dispatch) => (
  dispatch(reset("expense"))
);

ExpenseForm = reduxForm({
  form: "expense",
  validate,
  onSubmitSuccess: afterSubmit,
  enableReinitialize: true
})(withStyles(styles)(ExpenseForm));

ExpenseForm = connect(({categories}) => {
  let inValue = categories[0] ? categories[0].values.name : "";

  return {
    initialValues: {category: inValue}
  }
})(ExpenseForm);

function mapStateToProps({categories, expenses}) {
  return {categories, expenses}
}

export default connect(mapStateToProps, {addExpense, fetchExpenses})(ExpenseForm);
