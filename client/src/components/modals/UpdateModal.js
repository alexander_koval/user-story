import React, { Component } from "react";

import Modal from "@material-ui/core/Modal";
import HighLightOff from "@material-ui/icons/HighlightOff";
import Button from "../CustomButtons/Button";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import UpdateNameForm from "../forms/UpdateNameForm";

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
  close: {
    position: "absolute",
    top: "0.2rem",
    right: "0.2rem"
  },
  category: {
    minWidth: "2rem",
    minHeight: "1.5rem",
    cursor: "pointer"
  }
});

class UpdateModal extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, name, id } = this.props;

    return (
      <div style={{display: "inline-block"}}>
        <div
          className={classes.category}
          hidden={false}
          onClick={this.handleOpen}
        >
          {name}
        </div>
        <Modal
          open={this.state.open}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Button
              className={classes.close}
              justIcon
              simple
              color="danger"
              onClick={this.handleClose}
            >
              <HighLightOff/>
            </Button>
            <UpdateNameForm id={id} handleClose={this.handleClose}/>
          </div>
        </Modal>
      </div>
    );
  }
}

UpdateModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

const UpdateModalWrapped = withStyles(styles)(UpdateModal);

export default UpdateModalWrapped;