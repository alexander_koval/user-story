import React, { Component } from "react";

import Modal from "@material-ui/core/Modal";
import AcUnit from "@material-ui/icons/AcUnit";
import HighLightOff from "@material-ui/icons/HighlightOff";
import Button from "../CustomButtons/Button";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
  close: {
    position: "absolute",
    top: "0.2rem",
    right: "0.2rem"
  }
});

class CustomModal extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, children } = this.props;

    return (
      <div style={{display: "inline-block"}}>
        <Button hidden={false} color="info" onClick={this.handleOpen}><AcUnit/></Button>
        <Modal
          open={this.state.open}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Button
              className={classes.close}
              justIcon
              simple
              color="danger"
              onClick={this.handleClose}
            >
              <HighLightOff/>
            </Button>
            {children}
          </div>
        </Modal>
      </div>
    );
  }
}

CustomModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

const CustomModalWrapped = withStyles(styles)(CustomModal);

export default CustomModalWrapped;