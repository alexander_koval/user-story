import React, { Component } from "react";

import Modal from "@material-ui/core/Modal";
import HighLightOff from "@material-ui/icons/HighlightOff";
import Button from "../CustomButtons/Button";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Calendar from "react-calendar";
import GridContainer from "../Grid/GridContainer";
import GridItem from "../Grid/GridItem";

function getModalStyle() {

  return {
    top: `${50}%`,
    left: `${50}%`,
    transform: `translate(-${50}%, -${50}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 100,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
  close: {
    position: "absolute",
    top: "0.2rem",
    right: "0.2rem"
  }
});

class DateModal extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    return (
        <div style={{ display: "inline-block" }}>
          <Button hidden={false} color="primary" onClick={this.handleOpen}>Period</Button>
          <Modal
            open={this.state.open}
          >
            <div style={getModalStyle()} className={classes.paper}>
              <Button
                className={classes.close}
                justIcon
                simple
                color="danger"
                onClick={this.handleClose}
              >
                <HighLightOff/>
              </Button>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <h3>Set the start and end date of the period</h3>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Calendar
                    value={this.props.startDate}
                    onChange={val => this.props.setStartDate(val)}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                <Calendar
                  value={this.props.endDate}
                  onChange={val => this.props.setEndDate(val)}
                />
                </GridItem>
              </GridContainer>
            </div>
          </Modal>
        </div>
    );
  }
}

DateModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

const DateModalWrapped = withStyles(styles)(DateModal);

export default DateModalWrapped;