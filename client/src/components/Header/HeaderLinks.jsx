import React from 'react';
import classNames from 'classnames';
// import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { logoutUser } from '../../actions';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Hidden from '@material-ui/core/Hidden';
import Poppers from '@material-ui/core/Popper';
// @material-ui/icons
import Person from '@material-ui/icons/Person';
// core components
import Button from 'components/CustomButtons/Button.jsx';

import headerLinksStyle from 'assets/jss/material-dashboard-react/components/headerLinksStyle.jsx';

class HeaderLinks extends React.Component {
  state = {
    open: false
  };
  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    const { classes, auth } = this.props;
    const { open } = this.state;
    if (auth.user) {
      return (
        <div>
          <div className={classes.manager}>
            <span className={classes.buttonLink}>
              {auth.user ? auth.user.email : ''}
            </span>
            <Button
              buttonRef={node => {
                this.anchorEl = node;
              }}
              color={window.innerWidth > 959 ? 'transparent' : 'white'}
              justIcon={window.innerWidth > 959}
              simple={!(window.innerWidth > 959)}
              aria-owns={open ? 'menu-list-grow' : null}
              aria-haspopup="true"
              onClick={this.handleToggle}
              className={classes.buttonLink}
            >
              {auth.user.avatar !== '' ? (
                <img
                  className={classes.imgAvatar}
                  src={auth.user.avatar}
                  alt={auth.user.avatar}
                />
              ) : (
                <Person className={classes.icons} />
              )}
              <Hidden mdUp implementation="css">
                <p onClick={this.handleClick} className={classes.linkText}>
                  Notification
                </p>
              </Hidden>
            </Button>
            <Poppers
              open={open}
              anchorEl={this.anchorEl}
              transition
              disablePortal
              className={
                classNames({ [classes.popperClose]: !open }) +
                ' ' +
                classes.pooperNav
              }
            >
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  id="menu-list-grow"
                  style={{
                    transformOrigin:
                      placement === 'bottom'
                        ? 'center top'
                        : 'center bottom'
                  }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={this.handleClose}>
                      <MenuList role="menu">
                        <MenuItem
                          onClick={e => {
                            this.handleClose(e);
                            this.props.logoutUser(this.props.history);
                          }}
                          className={classes.dropdownItem}
                        >
                          <p
                            onClick={() =>
                              this.props.logoutUser(this.props.history)
                            }
                          >
                            Logout
                          </p>
                        </MenuItem>
                        <MenuItem
                          onClick={this.handleClose}
                          className={classes.dropdownItem}
                        >
                          <Link to="/profile">Change avatar</Link>
                        </MenuItem>
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Poppers>
          </div>
        </div>
      );
    } else {
      return <div />;
    }
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default withStyles(headerLinksStyle)(
  connect(
    mapStateToProps,
    { logoutUser }
  )(withRouter(HeaderLinks))
);
