import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
// core components
import footerStyle from "assets/jss/material-dashboard-react/components/footerStyle.jsx";

const Footer = ({ classes, routes }) => {
  const links = (
    <List className={classes.list}>
      {routes.map((route, key) => {
        if (route.redirect) return null;

        if (route.path === "/verify" || route.path === "/") return null;

        return (
          <ListItem key={key} className={classes.inlineBlock}>
            <a href={route.path} className={classes.block}>
              {route.sidebarName}
            </a>
          </ListItem>
        );
      })}
    </List>
  );

  return (
    <footer className={classes.footer}>
      <div className={classes.container}>
        <div className={classes.left}>
          {links}
        </div>
        <p className={classes.right}>
          <span>
            &copy; {1900 + new Date().getYear()}{" "}
            <a href="/" className={classes.a}>
              Kyzma
            </a>, made with love for a better web
          </span>
        </p>
      </div>
    </footer>
  );
};

Footer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(footerStyle)(Footer);
