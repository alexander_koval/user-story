import validateEmail from "./validateEmail";
import comparePassword from "./comparePassword";

export const validateSignPage = ({email, password, confirm_password}) => {
  const errors = {};

  if(!email) {
    errors.email = "This field is required! Please enter email."
  } else {
    if(validateEmail(email) === false) {
      errors.email = "You enter invalid email!"
    }
  }
  if(!password) {
    errors.password = "This field is required! Please enter password."
  } else {
    if(password.length < 4) {
      errors.password = "Your password to short!"
    }
    if(password.length > 12) {
      errors.password = "Your password to long!"
    }
  }
  if(!confirm_password) {
    errors.confirm_password = "This field is required! Please repeat password."
  } else {
    if(comparePassword(password, confirm_password)) {
      errors.confirm_password = "Confirm password dies not match!"
    }
  }

  return errors;
};
