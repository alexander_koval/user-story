export default (password, confirmPassword) => {

  return password !== confirmPassword;
}