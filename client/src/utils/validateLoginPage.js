import validateEmail from "./validateEmail";

export const validateLoginPage = ({email, password}) => {
  const errors = {};

  if(!email) {
    errors.email = "This field is required! Please enter email."
  } else {
    if(validateEmail(email) === false) {
      errors.email = "You enter invalid email!"
    }
  }
  if(!password) {
    errors.password = "This field is required! Please enter password."
  } else {
    if(password.length < 4) {
      errors.password = "Your password to short!"
    }
    if(password.length > 12) {
      errors.password = "Your password to long!"
    }
  }

  return errors;
};