import App from '../components/App';

const indexRoutes = [{ path: '/', component: App }];

export default indexRoutes;
