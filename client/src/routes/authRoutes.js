import Person from '@material-ui/icons/Person';

import LoginPage from '../pages/loginPage';
import SignPage from '../pages/signPage';
import VerifyPage from '../pages/verifyPage';
import WelcomePage from '../pages/welcomePage';

export default [
  {
    path: '/login',
    sidebarName: 'Sign in',
    navbarName: 'Sign in',
    icon: Person,
    component: LoginPage
  },
  {
    path: '/sign',
    sidebarName: 'Sign up',
    navbarName: 'Sign up',
    icon: Person,
    component: SignPage
  },
  {
    path: '/verify',
    sidebarName: 'Verify',
    navbarName: 'Email verification',
    icon: Person,
    component: VerifyPage
  },
  {
    path: '/',
    sidebarName: 'Verify',
    navbarName: 'Welcome',
    icon: Person,
    component: WelcomePage
  }
];
