import Dashboard from '@material-ui/icons/Dashboard';
import DashboardPage from '../pages/dashboardPage';
import Assignment from '@material-ui/icons/Assignment';
import ReportsPage from '../pages/reportsPage';
import Settings from '@material-ui/icons/Settings';
import ConfigPage from '../pages/configPage';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ProfilePage from '../pages/profilePage';

export default [
  {
    path: '/dashboard',
    sidebarName: 'Dashboard',
    navbarName: 'Dashboard',
    icon: Dashboard,
    component: DashboardPage
  },
  {
    path: '/reports',
    sidebarName: 'Reports',
    navbarName: 'Reports',
    icon: Assignment,
    component: ReportsPage
  },
  {
    path: '/config',
    sidebarName: 'Config',
    navbarName: 'Config',
    icon: Settings,
    component: ConfigPage
  },
  {
    path: '/profile',
    sidebarName: 'Profile',
    navbarName: 'Profile',
    icon: AccountCircle,
    component: ProfilePage
  },
  { redirect: true, path: '/', to: '/dashboard', navbarName: 'Redirect' }
];
