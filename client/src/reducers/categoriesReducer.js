import { FETCH_CATEGORIES_SUCCESS } from "../types/types";

const initialState = [];

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORIES_SUCCESS:
      return action.payload;

    default:
      return state;
  }
}