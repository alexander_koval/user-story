import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import authReducer from "./authReducer";
import expensesReducer from "./expensesReducer";
import categoriesReducer from "./categoriesReducer";

export default combineReducers({
  form: formReducer,
  auth: authReducer,
  expenses: expensesReducer,
  categories: categoriesReducer
});