import { FETCH_EXPENSES_SUCCESS } from '../types/types';

const initialState = [];

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_EXPENSES_SUCCESS:
      return action.payload;

    default:
      return state;
  }
}
