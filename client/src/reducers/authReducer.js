import {
  FETCH_USER,
  USER_LOGIN_FAILURE,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT_SUCCESS,
  USER_SIGN_FAILURE,
  USER_SIGN_SUCCESS,
  USER_VERIFY_FAILURE,
  USER_VERIFY_SUCCESS
} from '../types/types';

const initialState = {
  user: null,
  email: null,
  error: null
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USER:
      return { ...state, user: action.payload || null };

    case USER_SIGN_SUCCESS:
      return { ...state, email: action.payload, error: null };

    case USER_SIGN_FAILURE:
      return { ...state, error: action.payload };

    case USER_VERIFY_SUCCESS:
      return { ...state, error: null };

    case USER_VERIFY_FAILURE:
      return { ...state, error: action.payload };

    case USER_LOGIN_SUCCESS:
      return { ...state, error: null, user: action.payload };

    case USER_LOGIN_FAILURE:
      return { ...state, error: action.payload };

    case USER_LOGOUT_SUCCESS:
      return { ...state, user: null };

    default:
      return state;
  }
}
