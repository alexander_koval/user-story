import axios from 'axios';
import {
  FETCH_CATEGORIES_SUCCESS,
  FETCH_EXPENSES_SUCCESS,
  FETCH_USER,
  USER_LOGIN_FAILURE,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT_SUCCESS,
  USER_SIGN_FAILURE,
  USER_SIGN_SUCCESS,
  USER_VERIFY_FAILURE,
  USER_VERIFY_SUCCESS
} from '../types/types';

export const fetchUser = () => async dispatch => {
  const res = await axios.get('/api/current_user');

  dispatch({
    type: FETCH_USER,
    payload: res.data
  });
};

export const registerUser = (values, history) => async dispatch => {
  const res = await axios.post('/api/sign', values);

  if (res.data.error) {
    dispatch({
      type: USER_SIGN_FAILURE,
      payload: res.data.error
    });
  } else {
    dispatch({
      type: USER_SIGN_SUCCESS,
      payload: res.data
    });

    history.push(
      `/verify?email=${res.data.email}&check=${res.data.check}`
    );
  }
};

export const verifyUser = (values, history) => async dispatch => {
  const request = { email: values.email, checkcode: values.check };
  const res = await axios.post('/api/verify', request);

  if (res.data.error) {
    dispatch({
      type: USER_VERIFY_FAILURE,
      payload: res.data.error
    });
  } else {
    dispatch({
      type: USER_VERIFY_SUCCESS,
      payload: res.data
    });
    history.push('/login');
  }
};

export const loginUser = (values, history) => async dispatch => {
  try {
    const res = await axios.post('/api/login', values);
    dispatch({
      type: USER_LOGIN_SUCCESS,
      payload: res.data
    });

    history.push('/');
  } catch (e) {
    if (e.response) {
      dispatch({
        type: USER_LOGIN_FAILURE,
        payload: e.response.data
      });
    }
  }
};

export const logoutUser = history => dispatch => {
  axios.get('/api/logout');

  dispatch({
    type: USER_LOGOUT_SUCCESS
  });

  history.push('/login');
};

export const fetchCategories = () => async dispatch => {
  const res = await axios.get('/api/categories');

  dispatch({
    type: FETCH_CATEGORIES_SUCCESS,
    payload: res.data
  });
};

export const addCategory = () => async dispatch => {
  const res = await axios.post('/api/category/new');

  dispatch({
    type: FETCH_CATEGORIES_SUCCESS,
    payload: res.data
  });
};

export const updateCategory = (values, id) => async dispatch => {
  const name = values.name || '';
  const req = { name, id };

  const res = await axios.patch('/api/category/update', req);

  dispatch({
    type: FETCH_CATEGORIES_SUCCESS,
    payload: res.data
  });
};

export const moveUpCategory = id => async dispatch => {
  const res = await axios.post('/api/category/move/up', { id: id });

  dispatch({
    type: FETCH_CATEGORIES_SUCCESS,
    payload: res.data
  });
};

export const moveDownCategory = id => async dispatch => {
  const res = await axios.post('/api/category/move/down', { id });

  dispatch({
    type: FETCH_CATEGORIES_SUCCESS,
    payload: res.data
  });
};

export const deleteCategory = id => async dispatch => {
  const res = await axios.delete('/api/category/delete', { data: { id } });

  dispatch({
    type: FETCH_CATEGORIES_SUCCESS,
    payload: res.data
  });
};

export const fetchExpenses = () => async dispatch => {
  const res = await axios.get('/api/expenses');

  dispatch({
    type: FETCH_EXPENSES_SUCCESS,
    payload: res.data
  });
};

export const fetchExpensesFromDate = dates => async dispatch => {
  await axios.post('/api/expenses/date', dates);
  const res = await axios.get('/api/expenses');

  dispatch({
    type: FETCH_EXPENSES_SUCCESS,
    payload: res.data
  });
};

export const addExpense = values => async dispatch => {
  await axios.post('/api/expense/new', values);
  dispatch({
    type: FETCH_CATEGORIES_SUCCESS
  });
};

export const setAvatar = event => async dispatch => {
  event.preventDefault();

  let data = new FormData();
  const imagedata = document.querySelector('input[type="file"]').files[0];
  data.append('avatar', imagedata);
  if (!imagedata) return;

  await axios.post('/api/profile', data);

  const res = await axios.get('/api/current_user');

  dispatch({
    type: FETCH_USER,
    payload: res.data
  });
};
