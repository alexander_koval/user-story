import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchCategories, addCategory } from "../actions";

// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import GridItem from "../components/Grid/GridItem.jsx";
import GridContainer from "../components/Grid/GridContainer.jsx";
import Card from "../components/Card/Card.jsx";
import CardHeader from "../components/Card/CardHeader.jsx";
import CardBody from "../components/Card/CardBody.jsx";
import ConfigTable from "../components/Table/ConfigTable";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Button from "../components/CustomButtons/Button";
import CardFooter from "../components/Card/CardFooter";

class ConfigPage extends Component {
  state = {
    value: 0
  };


  componentDidMount() {
    this.props.fetchCategories();
  }


  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    const { classes, categories } = this.props;
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Edit Categories</h4>
              <p className={classes.cardCategoryWhite}>
                Please, config your categories
              </p>
            </CardHeader>
            <CardBody>
              <ConfigTable
                tableData={categories}
              />
            </CardBody>
            <CardFooter>
              <Button color="primary" onClick={this.props.addCategory}>Add category</Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

ConfigPage.propTypes = {
  classes: PropTypes.object.isRequired
};

function mapStateToProps({categories}) {
  return {categories}
}

export default withStyles(dashboardStyle)(connect(mapStateToProps, {fetchCategories, addCategory})(ConfigPage));