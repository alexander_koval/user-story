import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchCategories, fetchExpenses } from "../actions";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../components/Grid/GridItem.jsx";
import GridContainer from "../components/Grid/GridContainer.jsx";
import DashboardTable from "../components/Table/DashboardTable";
import Card from "../components/Card/Card.jsx";
import CardHeader from "../components/Card/CardHeader.jsx";
import CardBody from "../components/Card/CardBody.jsx";
import ExpenseForm from "../components/forms/ExpenseForm";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

class DashboardPage extends Component {

  componentDidMount() {
    this.props.fetchCategories();
    this.props.fetchExpenses();
  }

  render() {
    const { classes } = this.props;
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>New expenses</h4>
              <p className={classes.cardCategoryWhite}>
                Please enter new expenses
              </p>
            </CardHeader>
            <CardBody>
              <ExpenseForm/>
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>
                Latest expenses
              </h4>
              <p className={classes.cardCategoryWhite}>
                Here is 20 latest expenses
              </p>
            </CardHeader>
            <CardBody>
              <DashboardTable
                tableHeaderColor="primary"
                tableHead={["Data", "Category", "Expenses", "Value, UAH"]}
                tableData={this.props.expenses}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

function mapStateToProps({categories, expenses}) {
  return {categories, expenses}
}

export default withStyles(styles)(connect(mapStateToProps, {fetchCategories, fetchExpenses})(DashboardPage));

