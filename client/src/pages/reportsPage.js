import React, { Component } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { fetchExpensesFromDate } from '../actions';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
// core components
import GridItem from '../components/Grid/GridItem.jsx';
import GridContainer from '../components/Grid/GridContainer.jsx';
import ReportsTable from '../components/Table/ReportsTable';
import Card from '../components/Card/Card.jsx';
import CardHeader from '../components/Card/CardHeader.jsx';
import CardBody from '../components/Card/CardBody.jsx';
import Button from '../components/CustomButtons/Button';
import ArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import ViewList from '@material-ui/icons/ViewList';
import Equalizer from '@material-ui/icons/Equalizer';
import Graph from '../components/Graph/Graph';
import DateModal from '../components/modals/DateModal';

const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0'
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF'
    }
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1'
    }
  }
};

class ReportsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTable: true,
      startDate: new Date(
        new Date().getTime() - (new Date().getTime() % 86400000)
      ),
      endDate: new Date(),
      toggle: ''
    };
  }

  componentDidMount() {
    this.props.fetchExpensesFromDate({
      start: this.state.startDate,
      end: this.state.endDate
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.startDate !== this.state.startDate) return true;
    if (nextState.endDate !== this.state.endDate) return true;
    if (nextState.showTable !== this.state.showTable) return true;
    return nextProps.expenses !== this.props.expenses;
  }

  handleToggle = () => {
    this.setState({ showTable: !this.state.showTable });
  };

  addOneDay = () => {
    let edt = this.state.endDate;
    let sdt = this.state.startDate;

    switch (this.state.toggle) {
      case 'day':
        if (edt.getDate() === sdt.getDate()) {
          edt.setDate(edt.getDate() + 1);
          return this.setState({ endDate: edt });
        } else {
          edt.setDate(edt.getDate() + 1);
          sdt.setDate(sdt.getDate() + 1);
          return this.setState({ endDate: edt, startDate: sdt });
        }
      case 'week':
        if (edt.getDate() === sdt.getDate()) {
          edt.setDate(edt.getDate() + 7);
          return this.setState({ endDate: edt });
        } else {
          edt.setDate(edt.getDate() + 7);
          sdt.setDate(sdt.getDate() + 7);
          return this.setState({ endDate: edt, startDate: sdt });
        }
      case 'month':
        if (edt.getMonth() === sdt.getMonth()) {
          edt.setMonth(edt.getMonth() + 1);
          return this.setState({ endDate: edt });
        } else {
          edt.setMonth(edt.getMonth() + 1);
          sdt.setMonth(sdt.getMonth() + 1);
          return this.setState({ endDate: edt, startDate: sdt });
        }

      default:
        return this.setState({
          endDate: new Date()
        });
    }
  };

  backOneDay = () => {
    let edt = this.state.endDate;
    let sdt = this.state.startDate;

    switch (this.state.toggle) {
      case 'day':
        if (edt.getDate() === sdt.getDate()) {
          sdt.setDate(sdt.getDate() - 1);
          return this.setState({ startDate: sdt });
        } else {
          edt.setDate(edt.getDate() - 1);
          sdt.setDate(sdt.getDate() - 1);
          return this.setState({ endDate: edt, startDate: sdt });
        }
      case 'week':
        if (edt.getDate() === sdt.getDate()) {
          sdt.setDate(sdt.getDate() - 7);
          return this.setState({ startDate: sdt });
        } else {
          edt.setDate(edt.getDate() - 7);
          sdt.setDate(sdt.getDate() - 7);
          return this.setState({ endDate: edt, startDate: sdt });
        }
      case 'month':
        if (edt.getMonth() === sdt.getMonth()) {
          sdt.setMonth(sdt.getMonth() - 1);
          return this.setState({ startDate: sdt });
        } else {
          edt.setMonth(edt.getMonth() - 1);
          sdt.setMonth(sdt.getMonth() - 1);
          return this.setState({ endDate: edt, startDate: sdt });
        }

      default:
        return this.setState({
          endDate: new Date()
        });
    }
  };

  setDay = () => {
    this.setState({ toggle: 'day' });
  };

  setWeek = () => {
    this.setState({ toggle: 'week' });
  };

  setMonth = () => {
    this.setState({ toggle: 'month' });
  };

  setStartDate = date => {
    this.setState({ startDate: date });
  };

  setEndDate = date => {
    this.setState({ endDate: date });
  };

  render() {
    const { classes } = this.props;

    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="info">
              <h4 className={classes.cardTitleWhite}>Expenses reports</h4>
              <p className={classes.cardCategoryWhite}>
                Here is expenses reports
              </p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  {moment(this.state.startDate).format('ddd MMMM D YYYY')}/
                  {moment(this.state.endDate).format('ddd MMMM D YYYY')}
                </GridItem>
                <GridItem xs={12} sm={12} md={8}>
                  <Button
                    justIcon={true}
                    color="primary"
                    onClick={async () => {
                      await this.backOneDay();
                      await this.props.fetchExpensesFromDate({
                        start: this.state.startDate,
                        end: this.state.endDate
                      });
                    }}
                  >
                    <ArrowLeft />
                  </Button>
                  <Button
                    justIcon={true}
                    color="primary"
                    onClick={async () => {
                      await this.addOneDay();
                      await this.props.fetchExpensesFromDate({
                        start: this.state.startDate,
                        end: this.state.endDate
                      });
                    }}
                  >
                    <ArrowRight />
                  </Button>
                  <Button color="primary" onClick={this.setDay}>
                    Day
                  </Button>
                  <Button color="primary" onClick={this.setWeek}>
                    Week
                  </Button>
                  <Button color="primary" onClick={this.setMonth}>
                    Month
                  </Button>
                  <DateModal
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    setStartDate={this.setStartDate}
                    setEndDate={this.setEndDate}
                  />
                  <Button
                    justIcon
                    color="primary"
                    onClick={this.handleToggle}
                  >
                    {this.state.showTable ? <ViewList /> : <Equalizer />}
                  </Button>
                </GridItem>
              </GridContainer>
              {this.state.showTable ? (
                <ReportsTable
                  tableHeaderColor="primary"
                  tableHead={['Category', 'Expenses']}
                  tableData={this.props.expenses}
                />
              ) : (
                <Graph expenses={this.props.expenses} />
              )}
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

function mapStateToProps({ expenses }) {
  return { expenses };
}

export default withStyles(styles)(
  connect(
    mapStateToProps,
    { fetchExpensesFromDate }
  )(ReportsPage)
);
