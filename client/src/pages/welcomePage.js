import React from "react";
import { Link } from "react-router-dom";

import { withStyles } from "@material-ui/core/styles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle";

import GridContainer from "../components/Grid/GridContainer";
import GridItem from "../components/Grid/GridItem";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import money from "../assets/img/money.jpg";


const WelcomePage = () => {
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={10}>
        <Card >
          <CardActionArea style={{width: "100%" }}>
            <CardMedia
              style={{height: "250px" }}
              image={money}
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="display2" component="h2">
                Home Expenses
              </Typography>
              <Typography component="p">
                This application is designed to account for your daily expenses.
                If you have not registered yet, first register an account.
              </Typography>
              <Typography component="p">
                <strong>
                  Please indicate this account when registering as a confirmation code will be sent there.
                </strong>
              </Typography>
              <Typography>
                If you already have an account, go to the login page.
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary">
              <Link to="/login">Sign In</Link>
            </Button>
            <Button size="small" color="primary">
              <Link to="/sign">Sign Up</Link>
            </Button>
          </CardActions>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

export default withStyles(dashboardStyle)(WelcomePage);
