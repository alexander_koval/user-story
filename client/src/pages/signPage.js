import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "../components/Grid/GridItem.jsx";
import GridContainer from "../components/Grid/GridContainer.jsx";
import Card from "../components/Card/Card.jsx";
import CardHeader from "../components/Card/CardHeader.jsx";
import SignForm from "../components/forms/SignForm";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const SignPage = classes => {
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={8}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              Register with Home Expense App
            </h4>
            <p className={classes.cardCategoryWhite}>
              Please enter your email and password
            </p>
          </CardHeader>
          <SignForm />
        </Card>
      </GridItem>
    </GridContainer>
  );
};

export default withStyles(styles)(SignPage);
