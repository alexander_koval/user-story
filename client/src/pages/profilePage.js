import React from 'react';
import { connect } from 'react-redux';
import { setAvatar } from '../actions';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
// core components
import GridItem from '../components/Grid/GridItem.jsx';
import GridContainer from '../components/Grid/GridContainer.jsx';
import Button from '../components/CustomButtons/Button.jsx';
import Card from '../components/Card/Card.jsx';
import CardHeader from '../components/Card/CardHeader.jsx';
import CardAvatar from '../components/Card/CardAvatar.jsx';
import CardBody from '../components/Card/CardBody.jsx';

import avatar from 'assets/img/faces/marc.jpg';

const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0'
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none'
  }
};

const ProfilePage = props => {
  const { classes, auth, setAvatar } = props;
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Edit Profile</h4>
              <p className={classes.cardCategoryWhite}>
                Complete your profile
              </p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={8}>
                  <Card profile>
                    <CardAvatar profile>
                      <img
                        src={auth.user.avatar ? auth.user.avatar : avatar}
                        alt={
                          auth.user.avatar ? auth.user.avatar : 'avatar'
                        }
                      />
                    </CardAvatar>
                    <CardBody profile>
                      <form encType="multipart/form-data">
                        <input type="file" name="avatar" />
                        <h6 className={classes.cardCategory}>
                          {auth.user.email}
                        </h6>
                        <Button
                          type="button"
                          color="primary"
                          round
                          onClick={e => setAvatar(e)}
                        >
                          Change avatar
                        </Button>
                      </form>
                    </CardBody>
                  </Card>
                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
};
const ProfilePageWithStyle = withStyles(styles)(ProfilePage);
const mapStateToProps = ({ auth }) => ({ auth });

export default connect(
  mapStateToProps,
  { setAvatar }
)(ProfilePageWithStyle);
