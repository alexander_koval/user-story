module.exports = {
  mongoURI: process.env.MONGO_URI,
  port: process.env.PORT,
  cookieKey1: process.env.COOKIE_KEY_1,
  cookieKey2: process.env.COOKIE_KEY_2,
  mailUserName: process.env.MAIL_USER_NAME,
  mailUserPassword: process.env.MAIL_USER_PASSWORD,
  redirectDomain: process.env.REDIRECT_DOMAIN,
  mailApiKey: process.env.MAIL_API_KEY,
  mailSecretKey: process.env.MAIL_SECRET_KEY,
  sgApiKey: process.env.SG_API_KEY,
  sgSecretKey: process.env.SG_SECRET_KEY
};