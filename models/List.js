const _ = require('lodash');
const mongoose = require('mongoose');
const { Schema } = mongoose;
const Category = require('./Category');

const listSchema = new Schema({
  _user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  categories: {
    type: [Schema.Types.Object]
  }
});

listSchema.methods.addToTail = function() {
  let category = new Category();
  if (this.categories.length === 0) {
    this.categories.addToSet(category);
    this.save();
  } else {

    let insert = {};
    let index = false;
    _.map(this.categories, function(item, ind) {
      if (_.isNull(item.next)) {
        insert = _.clone(item);
        insert.next = category._id;
        index = ind;
      }
    });

    this.categories.set(index, insert);
    category.prev = insert._id;
    this.categories.addToSet(category);
    this.save();
  }
};

listSchema.methods.moveUp =  function(id) {
  let selected = _.find(this.categories, {_id: mongoose.Types.ObjectId(id)});
  if(_.isNull(selected.prev)) {
    return false
  } else {
    let moved = _.find(this.categories, {_id: mongoose.Types.ObjectId(selected.prev)});
    let indexSelected = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(id)});

    let movedChange = _.clone(moved);
    movedChange.values = selected.values || {};

    let selectedChange = _.clone(selected);
    selectedChange.values = moved.values || {};

    this.categories.set(indexSelected, selectedChange);
    indexMoved = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(selected.prev)});
    this.categories.set(indexMoved, movedChange);
    this.save();
  }
};

listSchema.methods.moveDown = function(id) {
  let selected = _.find(this.categories, {_id: mongoose.Types.ObjectId(id)});
  if(_.isNull(selected.next)) {
    return false
  } else {
    let moved = _.find(this.categories, {_id: mongoose.Types.ObjectId(selected.next)});
    let indexSelected = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(id)});

    let movedChange = _.clone(moved);
    movedChange.values = selected.values || {};

    let selectedChange = _.clone(selected);
    selectedChange.values = moved.values || {};

    this.categories.set(indexSelected, selectedChange);
    indexMoved = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(selected.next)});
    this.categories.set(indexMoved, movedChange);
    this.save();
  }
};

listSchema.methods.delete = function(id) {
  let deleteElem = _.find(this.categories, {_id: mongoose.Types.ObjectId(id)});
  if(deleteElem) {
    if(_.isNull(deleteElem.prev) && _.isNull(deleteElem.next)) {

      const removeIndex = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(id)});
      if(removeIndex !== -1) {
        this.categories.splice(removeIndex, 1);
      }

      this.save();

    } else if(_.isNull(deleteElem.prev)) {

      let nextElem = _.find(this.categories, {_id: mongoose.Types.ObjectId(deleteElem.next)});
      let index = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(deleteElem.next)});
      nextElem.prev = null;

      this.categories.set(index, nextElem);
      const removeIndex = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(id)});
      if(removeIndex !== -1) {
        this.categories.splice(removeIndex, 1);
      }
      this.save();

    } else if(_.isNull(deleteElem.next)) {

      let prevElem = _.find(this.categories, {_id: mongoose.Types.ObjectId(deleteElem.prev)});
      let index = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(deleteElem.prev)});
      prevElem.next = null;

      this.categories.set(index, prevElem);
      const removeIndex = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(id)});
      if(removeIndex !== -1) {
        this.categories.splice(removeIndex, 1);
      }
      this.save();

    } else {
      let prevElem = _.find(this.categories, { _id: mongoose.Types.ObjectId(deleteElem.prev)});
      let nextElem = _.find(this.categories, { _id: mongoose.Types.ObjectId(deleteElem.next)});
      let indexPrev = _.findIndex(this.categories, { _id: mongoose.Types.ObjectId(deleteElem.prev)});
      let indexNext = _.findIndex(this.categories, { _id: mongoose.Types.ObjectId(deleteElem.next)});

      prevElem.next = nextElem._id;
      this.categories.set(indexPrev, prevElem);

      nextElem.prev = prevElem._id;
      this.categories.set(indexNext, nextElem);

      let removeIndex = _.findIndex(this.categories, {_id: mongoose.Types.ObjectId(id)});
      if(removeIndex !== -1) {
        this.categories.splice(removeIndex, 1);
      }

      this.save();
    }
  }
};

mongoose.model('List', listSchema);

