const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
   type: String,
   required: true
  },
  verify: {
    type: Boolean,
    default: false
  },
  checkcode: {
    type: String
  },
  avatar: {
    type: String,
    default: ''
  }
});

mongoose.model('User', userSchema);