const mongoose = require('mongoose');
const { Schema } = mongoose;

const categorySchema = new Schema({
  values: {
    name: {
      type: String
    }
  },
  prev: {
    type: Schema.Types.ObjectId,
    default: null
  },
  next: {
    type: Schema.Types.ObjectId,
    default: null
  }
});

module.exports = mongoose.model('Category', categorySchema);