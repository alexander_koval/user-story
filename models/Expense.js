const mongoose = require('mongoose');
const { Schema } = mongoose;

const expenseSchema = new Schema({
  _user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  category: {
    type: String
  },
  description: {
    type: String
  },
  sum: {
    type: Number
  },
  date: {
    type: Date,
    default: new Date()
  }
});

mongoose.model('Expense', expenseSchema);