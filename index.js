const express = require('express');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const mongoose = require('mongoose');
const passport = require('passport');
const keys = require('./config/keys');

require('./models/User');
require('./models/List');
require('./models/Expense');
require('./services/passport');

mongoose.connect(keys.mongoURI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
});

const app = express();

app.use(bodyParser.json());

app.use(
  cookieSession({
    keys: [keys.cookieKey1, keys.cookieKey2],
    maxAge: 12 * 24 * 60 * 60 * 1000
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.set('views', './views');
app.set('view engine', 'pug');

require('./routes/authRoutes')(app);
require('./routes/categoriesRoutes')(app);
require('./routes/expenseRoutes')(app);

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));
  const path = require('path');

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}
app.use('/uploads', express.static('uploads'));

app.listen(keys.port);
