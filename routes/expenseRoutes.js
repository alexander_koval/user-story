const requireLogin = require('../middlewares/requireLogin');
const mongoose = require('mongoose');
const Expense = mongoose.model('Expense');

module.exports = app => {
  app.post('/api/expense/new', requireLogin, async (req, res) => {
    const { category, description, sum } = req.body;

    if (!sum) {
      return res.status(301).send({ error: 'You not entered value' });
    }

    let theSum = parseFloat(sum);

    if (isNaN(theSum)) {
      return res.status(301).send({ error: 'Entered data is not a valid!' });
    }

    let theSumFixed = Math.floor(theSum * 100) / 100;

    let expense = await new Expense({
      _user: req.user.id,
      category,
      description,
      sum: theSumFixed
    });
    expense.save();

    if (expense) res.status(200);
  });

  app.get('/api/expenses', requireLogin, async (req, res) => {
    let expenses = await Expense.aggregate([
      { $match: { _user: mongoose.Types.ObjectId(req.user.id) } },
      { $sort: { date: -1 } },
      { $limit: 20 }
    ]);

    res.send(expenses);
  });

  app.post('/api/expenses/date', requireLogin, async (req, res) => {
    let { start, end } = req.body;

    let expenses = await Expense.aggregate([
      {
        $match: {
          _user: mongoose.Types.ObjectId(req.user.id),
          date: { $gte: new Date(start), $lt: new Date(end) }
        }
      },
      { $sort: {
        date: 1
      }}
    ]);

    res.send(expenses);
  });
};
