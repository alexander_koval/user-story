const requireLogin = require('../middlewares/requireLogin');
const mongoose = require('mongoose');
const List = mongoose.model('List');

module.exports = app => {
  app.post('/api/category/new', requireLogin, async (req, res) => {
    let list = await List.findOne({ _user: req.user.id });
    if (!list) {
      list = new List({ _user: req.user.id });
      list.save();
    }
    list.addToTail();
    res.send(list.categories);
  });

  app.delete('/api/category/delete', requireLogin, async (req, res) => {
    let list = await List.findOne({ _user: req.user.id });
    if (list) {
      list.delete(req.body.id);
      res.send(list.categories);
    } else {
      res.send('failed');
    }
  });

  app.patch('/api/category/update', requireLogin, async (req, res) => {
    await List.updateOne(
      { 'categories._id': mongoose.Types.ObjectId(req.body.id) },
      { $set: { 'categories.$.values.name': req.body.name } }
    );

    let list = await List.findOne({ _user: req.user.id });
    res.send(list.categories);
  });

  app.post('/api/category/move/up', requireLogin, async (req, res) => {
    let list = await List.findOne({ _user: req.user.id });
    list.moveUp(req.body.id);
    res.send(list.categories);
  });

  app.post('/api/category/move/down', requireLogin, async (req, res) => {
    let list = await List.findOne({ _user: req.user.id });
    list.moveDown(req.body.id);
    res.send(list.categories);
  });

  app.get('/api/categories', requireLogin, async (req, res) => {
    let list = await List.findOne({ _user: req.user.id });

    const { categories } = list;
    res.send(categories);
  });
};
