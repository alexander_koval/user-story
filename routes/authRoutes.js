const passport = require('passport');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const checkEmail = require('../utils/checkEmail');
const { hashPassword } = require('../utils/hashing');
const randomString = require('randomstring');
const keys = require('../config/keys');

const multer = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
});
const upload = multer({storage: storage});
const requireLogin = require('../middlewares/requireLogin');

module.exports = app => {
  app.post('/api/login', (req, res, done) => {
    passport.authenticate('local', (err, user) => {
      if (err) return done(err);
      if (!user)
        return res
          .status(400)
          .send('Invalid email or password! Try again!');
      if (user.verify === false)
        return res
          .status(401)
          .send(
            'You have not passed verification by clicking the link in the letter at the email you provided'
          );
      req.logIn(user, err => {
        if (err) return res.status(401).send('Invalid data');
        const { _id, email, verify, avatar } = user;
        res.send({ _id, email, verify, avatar });
      });
    })(req, res, done);
  });

  app.post('/api/sign', async (req, res) => {
    const { email, password, confirm_password } = req.body;

    if (!email || !password || !confirm_password) {
      return res.send({ error: 'All fields is required' });
    }

    if (checkEmail(email) === false) {
      return res.send({ error: 'Enter a valid email' });
    }

    if (password.trim() !== '' && confirm_password.trim() !== '') {
      if (password.length < 4) {
        return res.send({ error: 'Your password is shortly' });
      }
      if (password !== confirm_password) {
        return res.send({
          error: 'Not match password and confirm password'
        });
      }
    } else {
      return res.send({
        error: 'You enter invalid password or confirm password'
      });
    }

    const checkUser = await User.findOne({ email: email });
    if (checkUser) {
      return res.send({ error: 'User with email already exist' });
    }

    const checkCode = randomString.generate(10);

    await new User({
      email: email,
      password: hashPassword(password),
      checkcode: checkCode
    }).save();

    res.send({email, check: checkCode});
  });

  app.post('/api/verify', async (req, res) => {
    const { email, checkcode } = req.body;

    const user = await User.findOneAndUpdate(
      { email: email, checkcode: checkcode, verify: false },
      { $set: { verify: true } }
    );

    if (!user) {
      return res.send({
        error:
          'Invalid email or code or you have already passed verification'
      });
    }
    res.send({ success: 'Your finish registration. Go to login page!' });
  });

  app.get('/api/logout', (req, res) => {
    req.logout();
    res.redirect('/login');
  });

  app.get('/api/current_user', (req, res) => {
    const {_id, email, verify, avatar} = req.user;
    res.send({_id, email, verify, avatar});
  });

  app.post('/api/profile', requireLogin, upload.single('avatar'), async (req, res, next) => {
    const file = req.file;
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
    }

    const user = await User.findOneAndUpdate(
      { _id: req.user._id },
      { $set: { avatar: file.path } }
    );
    if (!user) {
      return res.status(400);
    }
    
    const {_id, email, verify, avatar} = user;
    res.send({_id, email, verify, avatar});
  });
};
